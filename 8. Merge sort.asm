format PE console
entry start

include 'win32a.inc'

section '.data' data readable writable

	fmt    db '%s  ',10,0
	fmt1   db ' ',10,13,0
	fmt2   db '%d  ',0

	str0   db 'n-?',0

	nn     dd ?
	mass   dd ?

section '.code' code readable executable

start:
	cinvoke printf, fmt, str0
	cinvoke gets, nn
	cinvoke atoi, nn
	mov	[nn], eax
	mov	ebx, 4
	mul	ebx
	invoke	LocalAlloc, 0, eax

	mov	dword[mass], eax

	call	zap
	push	[nn]
	push	[mass]
	call	v1

	push	[mass]
	push	[nn]
	call	sort

	jmp	start

	invoke	LocalFree
	call	[getch]
	invoke	ExitProcess,0


proc zap;

	mov	ecx, [nn]
	push	ecx
	mov	edi, [mass]

	cinvoke time,0
	cinvoke srand,eax
	pop	ecx
@@:
	push	ecx
	cinvoke rand
	mov	ebx, 100
	div	ebx
	mov	eax, edx

	stosd
	pop	ecx
	loop @b
ret
endp


proc v1,m:DWORD, n:DWORD

     mov esi, [m]
     mov ecx, [n]

@@:
     push ecx
     lodsd
     cinvoke printf, fmt2, eax
     pop ecx
     loop @b
     cinvoke printf, fmt1
ret
endp


proc	sort, n:DWORD, m:DWORD

locals
	m1	dd ?
	m2	dd ?
	n1	dd ?
	n2	dd ?
	buf	dd ?
	bufc	dd ?
	count	dd 0
	x1	dd ?
	x2	dd ?
	c1	dd 1
	c2	dd 1
endl
	cmp	[n],1
	jle	sort1

	mov	eax,[m]
	mov	[m1], eax

	mov	eax,[n]
	mov	ebx,2
	xor	edx,edx
	div	ebx
	mov	[n1],eax

	mov	ebx,[n]
	sub	ebx,eax
	mov	[n2],ebx

	mov	ebx,4
	mul	ebx
	mov	ebx,[m1]
	add	eax,ebx
	mov	[m2],eax

	push	[m1]
	push	[n1]
	call	sort

	push	[m2]
	push	[n2]
	call	sort

	mov	eax,[n]
	mov	ebx,4
	mul	ebx
	invoke	LocalAlloc,0,eax
	mov	[buf],eax
	mov	[bufc],eax

	mov	esi,[m1]
	lodsd
	mov	[x1],eax

	mov	esi,[m2]
	lodsd
	mov	[x2],eax

sort4:
	mov	eax,[x2]
	cmp	[x1],eax
	jl	sort2

	mov	edi,[bufc]
	mov	eax,[x1]
	stosd
	inc	[count]
	mov	[bufc],edi

	inc	[c1]
	mov	eax,[n1]
	cmp	[c1],eax
	jg	sort3

	add	[m1],4
	mov	esi,[m1]
	lodsd
	mov	[x1],eax
	jmp	sort4

sort2:
	mov	edi,[bufc]
	mov	eax,[x2]
	stosd
	inc	[count]
	mov	[bufc],edi

	inc	[c2]
	mov	eax,[n2]
	cmp	[c2],eax
	jg	sort5
	add	[m2],4
	mov	esi,[m2]
	lodsd
	mov	[x2],eax
	jmp	sort4

sort3:
	mov	eax,[n2]
	cmp	[c2],eax
	jg	sort6
	mov	esi,[m2]
	lodsd
	mov	edi,[bufc]
	stosd

	inc	[c2]
	add	[m2],4

	mov	esi,[m2]
	lodsd
	mov	[x2],eax

	add	[bufc],4
	jmp	sort3

sort5:
	mov	eax,[n1]
	cmp	[c1],eax
	jg	sort6
	mov	esi,[m1]
	lodsd
	mov	edi,[bufc]
	stosd

	inc	[c1]
	add	[m1],4
	mov	esi,[m1]
	lodsd
	mov	[x1],eax
	add	[bufc],4

	jmp	sort5

sort6:
	mov	esi,[buf]

	mov	edi,[m]
	mov	ecx,[n]

@@:
	lodsd
	stosd
	loop	@b

	push	[n]
	push	[buf]
	call	v1

	push	[nn]
	push	[mass]
	call	v1

	invoke	LocalFree,[buf]

sort1:

ret
endp




section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess,	'ExitProcess',\
	       LocalAlloc,	'LocalAlloc',\
	       LocalFree,	'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       getch,  '_getch',\
	       gets,   'gets',\
	       atoi,   'atoi',\
	       time,   'time',\
	       srand,  'srand',\
	       rand,   'rand'

