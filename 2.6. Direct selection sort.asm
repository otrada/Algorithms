format PE console
entry start

include 'win32a.inc'

section '.data' data readable writable

	str0 db 'n - ?',0
	str1 db 10,10,10,'0 - exit, 1 - print, 2 - sort',0
	str2 db 'print element',0
	str3 db 'no such element',10,0
	str4 db 10,'-----',0

	fmt db '%s ',10,0
	fmt1 db 10,'%d ',0

	n dd 5
	nr dd ?
	x dd ?
	s rd 4
	sm rd 4
	m dd ?
	c dd 4
	strt dd ?
	count dd 0
	count1 dd 0
	mass rd 4
	     rd 4

section '.code' code readable executable

start:
	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	mov dword[mass], eax
	mov ebx, 4
	mul ebx
	invoke LocalAlloc, 0, eax
	mov dword[mass+4], eax


     mov ecx, dword[mass]
     mov [count1], ecx


     call zap
     call v

     jmp start2

qwe123:
	pop ecx
       cinvoke printf, fmt1, [count1]
       cinvoke printf, fmt1, [count]
       cinvoke printf, fmt, str4

       call v
       mov [count], 0
       jmp start



start2:
       mov eax, dword[mass]
       mov [nr],eax
       mov eax,dword[mass+4]
       mov [s], eax
       mov esi, dword[mass+4]
       mov ecx, dword[mass]

st22:
       push ecx
	    mov esi, [s]
	    lodsd
	    mov [m], eax
	    mov [strt], eax

	    mov ecx, [nr]
	    mov esi, [s]
 fgh:
	    push ecx
	    add [count], 1
	    lodsd
	    mov [x], eax
	    cmp eax, [m]
	    jl as12
		mov eax, [x]
		mov [m], eax
		mov [sm], esi
		sub [sm], 4
	 as12:
	    pop ecx
	    loop fgh


	mov eax, [m]
	mov edi, [s]
	stosd
	mov eax, [strt]
	mov edi, [sm]
	stosd

	add [s], 4
	sub [nr], 1
	pop ecx
	loop st22
	jmp qwe123



exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0


proc zap;

	mov ecx, dword[mass]

	push ecx
	mov edi, dword[mass+4]


	cinvoke time,0
	cinvoke srand, time
	pop ecx
@@:
	push ecx
	cinvoke rand
	mov ebx, 100
	div ebx
	mov eax, edx
	stosd
	pop ecx
	loop @b
ret
endp



proc v;

     mov esi, dword[mass+4]
     mov ecx, dword[mass]

@@:
     push ecx
     lodsd
     cinvoke printf, fmt1, eax
     pop ecx
     loop @b
     cinvoke printf, fmt, str4
ret
endp



section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       strcmp, 'strcmp',\
	       strlen, 'strlen',\
	       printf, 'printf',\
	       atoi, 'atoi',\
	       time, 'time',\
	       gets, 'gets',\
	       srand, 'srand',\
	       rand, 'rand',\
	       getch, '_getch'