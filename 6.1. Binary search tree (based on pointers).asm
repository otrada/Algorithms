format PE console
entry start

include 'win32a.inc'

section '.data' data readable writable

	str0   db 10,10,'1-print, 2-add, 3-delete, 4-=0mod2, 5->n, 6-sum, 7-max, 8-min, 9-print not even, 10-find, 11-median, 12-printl, 13-2num, 14-<0, 0-exit',0
	str1   db 'n - ?',0
	str2   db 'yes',0
	str3   db 'no',0

	fmt    db '%s  ',10,0
	fmt1   db '%d  ',0
	fmt2   db '%d  %d/%d',0

	root   dd ?
	x      dd ?
	k      dd ?
   ;     n      dd ?
	l:     dd 0
	       dd 0
	       dd 0
	u      dd 0
	w      dd 0
	count  dd 0
	count1 dd 0

section '.code' code readable executable

start:
	 mov	[root],0

startn:
	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x

	cmp	eax, 0
	je	exit

	cmp	eax, 1
	jne	c2

	call	check
	cmp	eax,1
	jne	startn

	push	[root]
	call	v
	jmp	startn

c2:
	cmp	eax, 2
	jne	c3

	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x
	mov	[k],eax

	call	check
	cmp	eax,0
	jne	cc2

	push	[k]
	push	root
	call	create
	jmp	startn
cc2:
	push	[k]
	push	[root]
	call	ad
	jmp	startn

c3:
	cmp	eax, 3
	jne	c4

	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x
	push	eax
	push	[root]
	call	find
	cmp	eax,0
	je	startn
	push	eax
	call	del
	jmp	startn

c4:
	cmp	eax,4
	jne	c5

	call	check
	cmp	eax,1
	jne	startn

	mov	[count],0
	push	[root]
	call	mod2
	cinvoke printf, fmt1,eax
	jmp	startn


c5:
	cmp	eax,5
	jne	c6

	call	check
	cmp	eax,1
	jne	startn

	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x
	mov	[k],eax

	mov	[count],0
	push	[k]
	push	[root]
	call	more
	cinvoke printf, fmt1,eax
	jmp	startn


c6:
	cmp	eax,6
	jne	c7

	call	check
	cmp	eax,1
	jne	startn

	mov	[count],0
	push	[root]
	call	sum
	cinvoke printf, fmt1,eax
	jmp	startn


c7:
	cmp	eax,7
	jne	c8

	call	check
	cmp	eax,1
	jne	startn

	mov	esi,[root]
	add	esi,4
	lodsd
	mov	[count],eax

	push	[root]
	call	max
	cinvoke printf, fmt1,[count]
	jmp	startn

c8:
	cmp	eax,8
	jne	c9

	call	check
	cmp	eax,1
	jne	startn

	mov	esi,[root]
	add	esi,4
	lodsd
	mov	[count],eax

	push	[root]
	call	min
	cinvoke printf, fmt1,[count]
	jmp	startn

c9:
	cmp	eax,9
	jne	c10

	call	check
	cmp	eax,1
	jne	startn

	push	[root]
	call	prne
	jmp	startn

c10:
	cmp	eax, 10
	jne	c11

	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x
	push	eax
	push	[root]
	call	find
	cmp	eax,0
	je	c101
	cinvoke printf, fmt, str2
	jmp	startn

c101:
	cinvoke printf, fmt, str3
	jmp	startn

c11:
	cmp	eax,11
	jne	c12

	call	check
	cmp	eax,1
	jne	startn

	mov	[count],0
	mov	[count1],0
	push	[root]
	call	sum
	mov	eax,[count]
	mov	ebx,[count1]
	xor	edx,edx
	div	ebx

	cinvoke printf, fmt2,eax,edx,eax
	jmp	startn

c12:
	cmp	eax, 12
	jne	c13

	call	check
	cmp	eax,1
	jne	startn

	push	[root]
	call	printl
	jmp	startn

c13:
	cmp	eax,13
	jne	c14

	call	check
	cmp	eax,1
	jne	startn

	mov	[count],0
	push	[root]
	call	num2
	cinvoke printf, fmt1,[count]
	jmp	startn

c14:
	cmp	eax,14
	jne	startn

	call	check
	cmp	eax,1
	jne	startn

	mov	[count],0
	push	[root]
	call	minn
	cmp	eax,1
	jne	c141
	cinvoke printf, fmt, str2
	jmp	startn
c141:
	cinvoke printf, fmt,  str3
	jmp	startn





exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0










;;;;;add

proc	create, n:DWORD,val:DWORD

locals
	c	dd ?
endl

	mov	eax,12
	invoke	LocalAlloc,0,eax
	mov	[c],eax
	mov	edi,[n]
	stosd

	mov	edi,[c]
	mov	eax, NULL
	stosd

	mov	eax, [val]
	stosd
	mov	eax,NULL
	stosd
ret
endp




proc	ad, n:DWORD, val:DWORD

locals
	c	dd ?
endl

	mov	esi,[n]
	lodsd
	lodsd
	cmp	eax,[val]
	jg	a1

	mov	esi,[n]
	add	esi,8
	mov	[c],esi
	lodsd
	jmp	tr

a1:
	mov	esi,[n]
	mov	[c],esi
	lodsd

tr:
	cmp	eax,NULL
	jne	ae

	push	[val]
	push	[c]

	call	create
	ret

ae:
	push   [val]
	push   eax
	call   ad

;endtr:
ret
endp






;;;;;print

proc	v, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	v1

	push	eax
	call	v

v1:
	mov	esi,[n]
	lodsd
	lodsd

	cinvoke printf, fmt1,eax

	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	ve

	push	eax
	call	v
	je	ve
ve:
ret
endp




proc	check
	mov	eax,0

	cmp	[root],0
	je	che
	mov	eax,1
che:
ret
endp


;;;;;del
proc	find,r:DWORD, val:DWORD

	cmp	[r],0
	jne	f0
	mov	eax,0
ret

f0:
	mov	esi,[r]
	lodsd
	lodsd
	cmp	[val],eax
	jne	f1
	mov	eax,[r]
ret

f1:

	jl	f2

	mov	esi,[r]
	add	esi,8
	mov	[u],esi

	lodsd
	jmp	de

f2:

	mov	esi,[r]
	mov	[u],esi
	lodsd

de:
	push	[val]
	push	eax
	call	find

ret
endp




proc	del, n:DWORD;, val:DWORD

locals
	e	dd 0
endl

	mov	 esi,[n]
	lodsd

	mov	ebx,[n]
	mov	[w],ebx
	mov	[e],eax

	cmp	 eax,0
	jne	 d0

	lodsd
	lodsd

	mov	edi,[u]
	stosd
ret

d0:
	mov	esi,[e]
	add	esi,8
	lodsd
	cmp	eax,0
	jne	d1

	mov	esi,[e]
	lodsd
	mov	edi,[n]
	stosd
	lodsd
	stosd
ret

d1:
	mov	esi,[e]
	mov	[w],esi
	add	esi,8
	lodsd
	mov	[e],eax

	mov	esi,eax
	add	esi,8
	lodsd

	cmp	eax,0
	jne	 d1


	mov	esi,[e]
	add	esi,4
	lodsd

	mov	edi,[n]
	add	edi,4
	stosd

	mov	esi,[e]
	lodsd

	mov	edi,[w]
	add	edi,8
	stosd
	invoke	LocalFree, [e]

ret
endp



;;;;;2. count
proc	mod2, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	mod1

	push	eax
	call	mod2

mod1:
	mov	esi,[n]
	lodsd
	lodsd

	xor	edx,edx
	mov	ebx,2
	div	ebx
	cmp	edx,0
	jne	mod3
	inc	[count]

mod3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	m

	push	eax
	call	mod2
   ;     je      m
m:
	mov	eax,[count]
ret
endp


;;;;;3. more
proc	more, n:DWORD, k:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	mor1

	push	[k]
	push	eax
	call	more

mor1:
	mov	esi,[n]
	lodsd
	lodsd

	cmp	eax,[k]
	jl	mor3

	inc	[count]

mor3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	mor4

	push	[k]
	push	eax
	call	more

mor4:
	mov	eax,[count]
ret
endp


;;;4. sum
proc	sum, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	sum1

	push	eax
	call	sum

sum1:
	mov	esi,[n]
	lodsd
	lodsd

	add	[count],eax
	inc	[count1]

sum3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	sum4

	push	eax
	call	sum
sum4:
	mov	eax,[count]
ret
endp


;;;5. max, min
proc	max, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	max1

	push	eax
	call	max

max1:
	mov	esi,[n]
	lodsd
	lodsd

	cmp	eax,[count]
	jl	max3

	mov	[count],eax

max3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	max4

	push	eax
	call	max

max4:
	mov	eax,[count]
ret
endp


proc	min, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	min1

	push	eax
	call	min

min1:
	mov	esi,[n]
	lodsd
	lodsd

	cmp	eax,[count]
	jg	min3

	mov	[count],eax

min3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	min4

	push	eax
	call	min

min4:
	mov	eax,[count]
ret
endp


;;;6. printnoteven
proc   prne, n:DWORD

locals
	c	dd ?
endl

   ;       cinvoke printf,fmt1,1
	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	pne1

	push	eax
	call	prne

pne1:
	mov	esi,[n]
	lodsd
	lodsd
	mov	[c],eax

	xor	edx,edx
	mov	ebx,2
	div	ebx
	cmp	edx,0
	je	pne3
	cinvoke printf,fmt1,[c]

pne3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	pne4

	push	eax
	call	prne
  ;      je      pne4
pne4:
ret
endp

;;; 10. printl

proc	printl, n:DWORD

	mov	esi,[n]
	add	esi,8
	lodsd

	cmp	eax,NULL
	je	printl1

	push	eax
	call	printl

printl1:
	mov	esi,[n]
	lodsd
	lodsd

	cinvoke printf, fmt1,eax

	mov	esi,[n]

	lodsd
	cmp	eax,NULL
	je	printle

	push	eax
	call	printl
	je	printle
printle:
ret
endp

;;;11. 2nun

proc	num2, n:DWORD
	  ;       cinvoke printf, fmt1,edx
	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	num1

	push	eax
	call	num2

num1:
	mov	esi,[n]
	lodsd
	lodsd
	cmp	eax,0
	jg	num11
	neg	eax

num11:
	xor	edx,edx
	mov	ebx,100
	idiv	 ebx
	cmp	eax,0
	jne	num3


	mov	eax,edx

	xor	edx,edx
	mov	ebx,10
	idiv	 ebx
	cmp	eax,0
	je	num3

	inc	[count]

num3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	num4

	push	eax
	call	num2
   ;     je      m
num4:
	mov	eax,[count]
ret
endp


proc	minn, n:DWORD

	mov	esi,[n]
	lodsd

	cmp	eax,NULL
	je	minn1

	push	eax
	call	minn

minn1:
	mov	esi,[n]
	lodsd
	lodsd

	cmp	eax,0
	jge	minn3
	mov	 eax,1
	inc	 [count]
	ret


minn3:
	mov	esi,[n]
	add	esi,8
	lodsd
	cmp	eax,NULL
	je	minn4

	push	eax
	call	minn
   ;     je      m
minn4:
	mov	eax,[count]
ret
endp




section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess,	'ExitProcess',\
	       LocalAlloc,	'LocalAlloc',\
	       LocalFree,	'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       gets,   'gets',\
	       atoi,   'atoi',\
	       getch,  '_getch'

