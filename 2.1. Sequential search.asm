format PE console
entry start

include 'win32a.inc'

section '.data' data readable writeable

	str0 db 'n - ?',0
	str1 db 'print element',0
	str2 db 'yes',0
	str3 db 'no',0

	fmt db '%s ',10,0
	fmt1 db '%d ',10,0

	c dd 4
	x rb 20
	y dd ?
	z dd ?
	s dd ?
	mass rd 4
	     rd 4

section '.code' code readable executable

start:
	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	mov dword[mass], eax
	mul [c]
	invoke LocalAlloc, 0, eax
	mov dword[mass+4], eax

	 push mass
	 call zap

	 push mass
	 call v

entr:
	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x
	mov [y], eax

	mov ecx, dword[mass]
	mov eax, dword[mass+4]
	mov [s], eax

sta:
	push ecx
	mov esi, [s]
	lodsd
	mov ebx, [y]

	cmp eax, ebx
	je en
	pop ecx
	mov ebx, [c]
	add [s], ebx
	loop sta
	cinvoke printf, fmt, str3
	jmp entr

en:
	cinvoke printf, fmt, str2
	jmp entr


proc zap;

	mov ecx, dword[mass]

	push ecx
	mov edi, dword[mass+4]


	cinvoke time,0
	cinvoke srand, time
	pop ecx
@@:
	push ecx
	cinvoke rand
	mov ebx, 100
	div ebx
	mov eax, edx
	stosd
	pop ecx
	loop @b
ret
endp



proc v;

     mov esi, dword[mass+4]
     mov ecx, dword[mass]

@@:
     push ecx
     lodsd
     cinvoke printf, fmt1, eax
     pop ecx
     loop @b
ret
endp





section '.idata' import data readable
	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       gets, 'gets',\
	       atoi, 'atoi',\
	       time, 'time',\
	       srand, 'srand',\
	       rand, 'rand',\
	       strcmp, 'strcmp'