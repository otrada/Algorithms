format PE console
entry start

include 'win32a.inc'


section '.data' data readable writable

	str_ db 'l - ?',0
	str0 db 'n - ?',0
	str1 db 10,13,10,13,10,13,'0 - exit, 1 - print, 2 - add, 3 - delete',0
	str2 db 'print element',10,13,0

	fmt db '%s ',10,13,0
	fmt0 db '%s',0
	fmt1 db 10,13,'%d ',0

	x rd 20
	n rb 4
	s dd ?
	c dd ?
	stek rd 4
	     rd 4





section '.code' code readable executable
start:
     cinvoke printf, fmt, str_
     cinvoke gets, x
     cinvoke atoi, x
     mov [c], eax

     cinvoke printf, fmt, str0
     cinvoke gets, x
     cinvoke atoi, x
     mov dword[stek], eax



     mov ebx, [c]
     mul ebx
     invoke LocalAlloc, 0, eax

     mov dword[stek+4], eax
     mov dword[stek], 0

entr:

     cinvoke printf, fmt, str1
     cinvoke gets, x
     cinvoke atoi, x

     cmp eax,0
     je exit
     cmp eax,1
     je start1
     cmp eax,2
     je start2
     cmp eax,3
     je start3
     jmp entr




start1:
       mov ecx, dword[stek]
       cmp ecx, 0
       je entr

       mov eax, dword[stek+4]
       mov [s],eax
       mov ebx, [c]
       add [s], ebx

sta1:
	push ecx
	cinvoke printf, fmt, [s]

	mov ebx, [c]
	add [s],ebx
	pop ecx
	loop sta1
	jmp entr



start2:
       add dword[stek], 1
       cinvoke printf, fmt0, str2
       cinvoke gets, x
       mov eax,dword[stek]

       mov ebx, [c]
       mul ebx

       mov ebx, dword[stek+4];;stek
       add eax, ebx
       mov [s], eax


       cinvoke strlen, x
       mov ecx, eax
       mov edi,[s]
       mov esi,x
       cld
       rep movsb
       mov eax, 0
       stosd
       jmp entr




start3:
    sub dword[stek], 1
    jmp entr


exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0




section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	scanf, 'scanf',\
	strlen, 'strlen',\
	       strcpy, 'strcpy',\
	       printf, 'printf',\
	       atoi, 'atoi',\
	       gets, 'gets',\
	       getch, '_getch'