format PE console
entry start

include 'win32a.inc'

section '.data' data readable writable

	str0 db 'n - ?',0
	str4 db 10,'-----',0

	fmt db '%s ',10,0
	fmt1 db '%d  ',0

	nr dd ?
	nr1 dd ?
	x dd ?
	x1 dd ?
	x2 dd ?
	sx1 dd ?
	sx2 dd ?
	c dd 4
	mass rd 4
	     rd 4


section '.code' code readable executable

start:
	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	mov dword[mass], eax
	mov ebx, 4
	mul ebx
	invoke LocalAlloc, 0, eax
	mov dword[mass+4], eax


     mov ecx, dword[mass]

     call zap
     call v

     jmp start2

qwe123:
	pop ecx
	cinvoke printf, fmt, str4

	call v
	jmp start





start2:
       mov eax, dword[mass]
       mov [nr], eax

st22:
	dec [nr]
		cinvoke printf, fmt1,[nr]
		cinvoke printf, fmt, str4

	mov esi, dword[mass+4]
	mov [sx2], esi
	    lodsd
	mov [x2], eax


	mov eax, [nr]
	mov [nr1], eax
 @@:
	    dec [nr1]

	    mov eax, [x2]
	    mov [x1], eax
	    mov eax, [sx2]
	    mov [sx1], eax

	    mov [sx2], esi
	    lodsd
	    mov [x2], eax

	    mov eax, [x2]
	    cmp [x1], eax
	    jl as12

		mov eax, [x1]
		mov edi, [sx2]
		stosd

		mov eax, [x2]
		mov edi, [sx1]
		stosd


	     push esi
	     call v
	     pop esi

	    mov eax, [x1]
	    mov [x2], eax

as12:
	    cmp [nr1], 0

	    jg @b

       cmp [nr], 0

       jg st22

       jmp qwe123
exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0




proc zap;

	mov ecx, dword[mass]

	push ecx
	mov edi, dword[mass+4]


	cinvoke time,0
	cinvoke srand, time
	pop ecx
@@:
	push ecx
	cinvoke rand
	mov ebx, 100
	div ebx
	mov eax, edx
	stosd
	pop ecx
	loop @b
ret
endp



proc v;

     mov esi, dword[mass+4]
     mov ecx, dword[mass]

@@:
     push ecx
     lodsd
     cinvoke printf, fmt1, eax
     pop ecx
     loop @b
     cinvoke printf, fmt, str4
ret
endp


section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       strcmp, 'strcmp',\
	       strlen, 'strlen',\
	       printf, 'printf',\
	       atoi, 'atoi',\
	       time, 'time',\
	       gets, 'gets',\
	       srand, 'srand',\
	       rand, 'rand',\
	       getch, '_getch'
