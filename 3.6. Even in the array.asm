format PE console
entry start

include 'win32a.inc'

section '.data' data readable writeable

	str0 db 'n - ?',0
	str1 db 10,'----',10,0

	fmt db '%s ',10,0
	fmt0 db ' %d',10,0
	fmt1 db '%d  %d',10,0

	c dd 0
	x rb 20

	s dd 0
	mass rd 4
	     rd 4

section '.code' code readable executable

start:
	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	mov dword[mass], eax
	mov ebx, 4
	mul ebx
	invoke LocalAlloc, 0, eax
	mov dword[mass+4], eax


	call zap
	call v
       cinvoke printf, fmt, str1


	mov ecx, dword[mass]
	mov esi, dword[mass+4]
@@:
	push ecx
	lodsd
	mov ebx, 2
	xor edx, edx
	div ebx
	cmp edx, 0
	jne stqwe
	  add [s], 1

stqwe:
	add [c], 1
	cinvoke printf, fmt1, [c], [s]
	pop ecx
	loop @b


	invoke LocalFree
	call [getch]
	invoke ExitProcess,0





proc zap;

	mov ecx, dword[mass]

	push ecx
	mov edi, dword[mass+4]


	cinvoke time,0
	cinvoke srand, time
	pop ecx
@@:
	push ecx
	cinvoke rand
	mov ebx, 21
	div ebx
	mov eax, edx
	stosd
	pop ecx
	loop @b
ret
endp



proc v;

     mov esi, dword[mass+4]
     mov ecx, dword[mass]

@@:
     push ecx
     lodsd
     cinvoke printf, fmt0, eax
     pop ecx
     loop @b
ret
endp





section '.idata' import data readable
	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       gets, 'gets',\
	       atoi, 'atoi',\
	       time, 'time',\
	       srand, 'srand',\
	       rand, 'rand',\
	       getch, '_getch'
;		strcmp, 'strcmp'