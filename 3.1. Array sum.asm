format PE console
entry start

include 'win32a.inc'

section '.data' data readable writeable

	fmt1 db '%d %d',10,0
	c dd 1
	s dd 0

section '.code' code readable executable

start:
	mov eax, [c]
	add [s], eax
	cinvoke printf, fmt1, [c], [s]
	add [c], 1
	cmp [c], 20
	jg exit
	jmp start


exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0



section '.idata' import data readable
	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       getch, '_getch'