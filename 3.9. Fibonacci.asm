format PE console
entry start

include 'win32a.inc'

section '.data' data readable writeable

	str0 db 'n - ?',0
	str1 db 10,'----',10,0
	fmt0 db '%s ',10,0
	fmt1 db '%d %d',10,0
	c dd 0
	s dd 0
	x dd ?
	y dd ?
	n dd ?

section '.code' code readable executable
start:

	cinvoke printf, fmt0, str0
	cinvoke gets, x
	cinvoke atoi, x
	mov [n], eax

	mov [x], 1
	mov [y], 1
	mov ecx, [n]

@@:
	push ecx
	mov eax, [x]
	mov ebx, [y]
	add eax, ebx
	mov [s], eax

	mov eax, [y]
	mov [x], eax

	mov eax, [s]
	mov [y], eax

	add [c], 1
	cinvoke printf, fmt1, [c], [s]
	pop ecx
	loop @b


exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0



section '.idata' import data readable
	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       gets, 'gets',\
	       atoi, 'atoi',\
	       getch, '_getch'