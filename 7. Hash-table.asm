format PE console
entry start

include 'win32a.inc'

section '.data' data readable writable


	fmt    db '%s  ',10,0
	fmt1   db ' ',10,13,0
	fmt2   db '%d  ',0
	fmt3   db 13,10,'%d -> ',0
	fmt4   db '%d  %d/%d ',10,0

	str0   db 'n-?',0
	str1   db 13,10,10,'0-exit, 1-print, 2-add, 3-larger, 4-sum, 5-max, 6-min, 7-print not even, 8-find, 9-median, 10-num2',10,0

	x      dd ?
	root   dd ?
	n      dd 10

section '.code' code readable executable

start:
	mov	eax,[n]
	mov	ebx,4
	mul	ebx
	cinvoke LocalAlloc, 0, eax
	mov	[root],eax
	mov	 edi,eax
	mov	 ecx,10


startx:
	push	ecx
	mov	 eax,0
	stosd
	pop	ecx
	loop	startx

	cinvoke time,0
	cinvoke srand, time
	mov	ecx,[n]

startgen:
	push	ecx

	cinvoke rand
	mov	ebx, 200
	div	ebx

	push	edx
		cinvoke printf, fmt2,edx
	push	[root]
	call	ad

	pop	ecx
	loop	startgen


	push	[root]
	call	v


startz:
	cinvoke printf, fmt, str1
	cinvoke gets, x
	cinvoke atoi, x

	cmp	eax, 0
	je	exit

	cmp	eax, 1
	jne	c2

	push	[root]
	call	check
	cmp	eax,1
	jne	startz

	push	[root]
	call	v
	jmp	startz

c2:
	cmp	eax, 2
	jne	c3

	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x

	push	eax
	push	[root]
	call	ad

	push	[root]
	call	v
	jmp	startz

c3:
	cmp	eax, 3
	jne	c4

	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	push	eax
	push	[root]
	call	larger
	cinvoke printf, fmt2,eax
	jmp	startz

c4:
	cmp	eax, 4
	jne	c5

	push	[root]
	call	sum
	cinvoke printf, fmt2,eax
	jmp	startz

c5:
	cmp	eax, 5
	jne	c6

	push	[root]
	call	max
	cinvoke printf, fmt2,eax
	jmp	startz

c6:
	cmp	eax, 6
	jne	c7

	push	[root]
	call	min
	cinvoke printf, fmt2,eax
	jmp	startz

c7:
	cmp	eax, 7
	jne	c8

	push	[root]
	call	neven
	jmp	startz

c8:
	cmp	eax, 8
	jne	c9

	cinvoke printf, fmt, str0
	cinvoke gets, x
	cinvoke atoi, x
	push	eax
	push	[root]
	call	find
	cinvoke printf, fmt2,eax
	jmp	startz
c9:
	cmp	eax, 9
	jne	c10

	push	[root]
	call	median
	jmp	startz

c10:
	cmp	eax, 10
	jne	startz

	push	[root]
	call	num22
	cinvoke printf,fmt2,eax
	jmp	startz

exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0




;;;;;add
proc	ad, nr:DWORD, val:DWORD

locals
	c	dd ?
	d	dd ?
endl
	xor	edx,edx
	mov	eax,[val]
	div	[n]
	mov	eax,edx
	mov	ebx,4
	mul	ebx

	mov	esi,[nr]
	add	esi,eax
	mov	[c],esi

ad1:
	lodsd
	cmp	eax,0
    ;
	je	ad2
	mov	esi,eax
	mov	[c],esi
	jmp	ad1

ad2:
	mov	eax,8
	cinvoke LocalAlloc,0,eax
	mov	edi,eax
	mov	[d],eax
	mov	eax,0
	stosd
	mov	eax,[val]
	stosd

	mov	edi,[c]
	mov	eax,[d]

	stosd
ret
endp


;;;;;print
proc	v, n:DWORD

locals
	place	dd ?
	p	dd 0
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

pr1:
	push	ecx
	cinvoke printf, fmt3,[p]
	mov	esi,[place]
	lodsd

pr2:
	cmp	eax,0
	je	pr3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	cinvoke printf, fmt2,eax
	mov	esi,[pl]
	lodsd
	jne	pr2

pr3:
	add	[place],4
	inc	[p]

	pop	ecx
	loop	pr1
	cinvoke printf, fmt1,eax

ret
endp




proc	check, n:DWORD

locals
	place dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

ch1:
	push	ecx
	lodsd

	cmp	eax,0
	jne	ch2
	mov	eax,1
	ret
ch2:
	pop	ecx
	loop	ch1

	mov	eax,0
ret
endp

;;;3-larger
proc	larger, n:DWORD, val:dword

locals
	count	dd 0
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

lar1:
	push	ecx
	mov	esi,[place]
	lodsd

lar2:
	cmp	eax,0
	je	lar3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	cmp	eax, [val]
	jle	 lar4
	inc	[count]
lar4:
	mov	esi,[pl]
	lodsd
	jne	lar2

lar3:
	add	[place],4

	pop	ecx
	loop	lar1

	mov	eax,[count]
ret
endp


;;;4-sum
proc	sum, n:DWORD

locals
	count	dd 0
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

sum1:
	push	ecx
	mov	esi,[place]
	lodsd

sum2:
	cmp	eax,0
	je	sum3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	add	[count],eax
	mov	esi,[pl]
	lodsd
	jne	sum2

sum3:
	add	[place],4

	pop	ecx
	loop	sum1

	mov	eax,[count]
ret
endp

;;;5-max-min
proc	max, n:DWORD

locals
	count	dd -4444
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

max1:
	push	ecx
	mov	esi,[place]
	lodsd

max2:
	cmp	eax,0
	je	max3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	cmp	eax, [count]
	jle	max4
	mov	[count],eax
max4:
	mov	esi,[pl]
	lodsd
	jne	max2

max3:
	add	[place],4

	pop	ecx
	loop	max1

	mov	eax,[count]
ret
endp


proc	min, n:DWORD

locals
	count	dd 7777
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

min1:
	push	ecx
	mov	esi,[place]
	lodsd

min2:
	cmp	eax,0
	je	min3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	cmp	eax, [count]
	jge	min4

	mov	[count],eax
min4:
	mov	esi,[pl]
	lodsd
	jne	min2

min3:
	add	[place],4

	pop	ecx
	loop	min1

	mov	eax,[count]
ret
endp


;;;6-printnoteven
proc	neven, n:DWORD

locals
	place	dd ?
	p	dd 1
	pl	dd ?
	temp	dd ?
endl

	mov	esi,[n]
	add	esi,4
	mov	[place],esi
	mov	ecx,5

nev1:
	push	ecx
	cinvoke printf, fmt3,[p]
	mov	esi,[place]
	lodsd

nev2:
	cmp	eax,0
	je	nev3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd

	mov	[temp],eax
	mov	ebx,2
	xor	edx,edx
	div	ebx
	cmp	edx,1
	jne	nev4

	cinvoke printf, fmt2,[temp]

nev4:
	mov	esi,[pl]
	lodsd
	jne	nev2

nev3:
	add	[place],8
	inc	[p]

	pop	ecx
	loop	nev1
	cinvoke printf, fmt1

ret
endp

;;;7-fimd
proc	find, nr:DWORD, val:dword

locals
	d	dd ?
endl
	xor	edx,edx
	mov	eax,[val]
	div	[n]
	mov	eax,edx
	mov	ebx,4
	mul	ebx

	mov	esi,[nr]
	add	esi,eax

f1:
	lodsd
	mov	[d],eax
	lodsd
	cmp	eax,[val]
	jne	f3

	mov	eax,1
	ret
f3:
	cmp	[d],0
	je	f2
	mov	esi,[d]
	jmp	f1

f2:
	mov	eax,0

ret
endp


;;;8-median
proc	median, n:DWORD

locals
	count	dd 0
	count1	dd 0
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

med1:
	push	ecx
	mov	esi,[place]
	lodsd

med2:
	cmp	eax,0
	je	med3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd
	add	[count],eax
	inc	[count1]
	mov	esi,[pl]
	lodsd
	jne	med2

med3:
	add	[place],4

	pop	ecx
	loop	med1

	mov	eax,[count]
	mov	ebx,[count1]
	xor	edx,edx
	div	ebx
	cinvoke printf,fmt4,eax,edx,[count1]
ret
endp


proc	num22, n:DWORD

locals
	count	dd 0
	place	dd ?
	pl	dd ?
endl

	mov	esi,[n]
	mov	[place],esi
	mov	ecx,10

num1:
	push	ecx
	mov	esi,[place]
	lodsd

num2:
	cmp	eax,0
	je	num3
	mov	esi,eax
	mov	[pl],esi
	add	esi,4
	lodsd

	cmp	eax,10
	jl	num4
	cmp	eax,99
	jg	num4

	inc	[count]
num4:
	mov	esi,[pl]
	lodsd
	jne	num2

num3:
	add	[place],4

	pop	ecx
	loop	num1

	mov	eax,[count]
ret
endp


section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess,	'ExitProcess',\
	       LocalAlloc,	'LocalAlloc',\
	       LocalFree,	'LocalFree'

	import msvcrt,\
	       printf, 'printf',\
	       getch,  '_getch',\
	       gets,   'gets',\
	       atoi,   'atoi',\
	       time,   'time',\
	       srand,  'srand',\
	       rand,   'rand'


