format PE console
entry start

include 'win32a.inc'


section '.data' data readable writable

	str0 db 'n - ?',0
	str1 db 10,10,10,'0 - exit, 1 - print, 2 - add, 3 - find, 4 - delete',0
	str2 db 'print element',0
	str3 db 'no such element',10,0

	fmt db '%s ',10,0
	fmt1 db 10,'%d ',0

	n dd ?
	nr dd ?
	c dd 10
	x rb 20
	sm rd 4

	mass rd 4
	     rd 4

section '.code' code readable executable
start:
     cinvoke printf, fmt, str0
     cinvoke gets, x
     cinvoke atoi, x
     mov [n], eax
     invoke LocalAlloc, 0, [n]
     mov dword[mass], eax

     mov eax, [c]
     mov ebx, [n]
     mul ebx
     invoke LocalAlloc, 0, eax
     mov dword[mass+4], eax

     mov edi, [mass]
     mov ecx, [n]
@@:
	mov eax, 0
	stosd
	loop @b

	mov [nr], 0

entr:
     cinvoke printf, fmt, str1
     cinvoke gets, x
     cinvoke atoi, x

     cmp eax, 0
     je exit
     cmp eax, 1
     je start1
     cmp eax,2
     je start2
     cmp eax, 3
     je start3
     cmp eax,4
     je start4
     cinvoke printf, fmt, str3
     jmp entr




start1:
       mov esi, dword[mass]
       mov [nr], -1
       mov ecx, [n]
sta1:
	push ecx
	add [nr], 1
	lodsb
	cmp eax, 1
	jne en1

	mov ebx, [nr]
	mov eax, [c]
	mul ebx
	mov ebx, dword[mass+4]
	add eax, ebx
	cinvoke printf, fmt, eax
en1:
	pop ecx
	loop sta1
	jmp entr




start2:
       mov esi, dword[mass]
       mov ecx, [n]
       mov [nr], 0
sta2:
	push ecx
	add [nr], 1
	lodsb
	cmp al, 0  ;;;
	je en2

	pop ecx
	loop sta2
en2:
	sub esi, 1
	mov al, 1
	mov edi, esi
	stosb

	sub [nr], 1
	mov ebx, [nr]

	mov eax, [c]
	mul ebx
	mov ebx, dword[mass+4]
	add eax, ebx
	mov [sm], eax

	cinvoke printf, fmt, str2
	cinvoke gets, x
	cinvoke atoi,x

       cinvoke strlen, x
       mov ecx, eax
       mov edi,[sm]
       mov esi,x
       cld
       rep movsb
       mov al, 0
       stosb
       jmp entr



start3:
	cinvoke printf, fmt, str2
	cinvoke gets, x

	mov ecx, [n]
	mov eax, dword[mass+4]
	mov [sm], eax
	mov [nr],0
sta3:
	push ecx
	add [nr], 1
	cinvoke strcmp, x, [sm]
	mov ebx, [c]
	add [sm], ebx

	cmp eax, 0
	je en3
	pop ecx
	loop sta3

	cinvoke printf, fmt, str3
	jmp entr
en3:
	mov esi, dword[mass]
	mov ebx, [nr]

	add esi, ebx
	sub esi, 1
	lodsb
	cmp al, 1
	 pop ecx
	jne sta3
	cinvoke printf, fmt1, [nr]
	jmp entr



start4:
	cinvoke printf, fmt, str2
	cinvoke gets, x
	cinvoke atoi,x
	mov ebx, dword[mass]
	sub eax, 1
	add eax, ebx
	mov edi, eax
	mov eax, 0
	stosb
	jmp entr




exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0




section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       strcmp, 'strcmp',\
	       strlen, 'strlen',\
	       printf, 'printf',\
	       atoi, 'atoi',\
	       gets, 'gets',\
	       getch, '_getch'