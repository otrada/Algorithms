
format PE console
entry start

include 'win32a.inc'


section '.data' data readable writable

	str0 db 'n - ?',0
	str1 db 10,10,10,'0 - exit, 1 - print, 2 - add, 3 - delete',0

	fmt	db '  ',0
	fmt1	db '%2d',0
	fmt2	db '',13,10,0
	fmt3	db '%s ',10,0

	n	dd ?
	amount	dd 0
	x	dd ?

	mass	rd 4
		rd 4

section '.code' code readable executable
start:
     mov	[mass],16
     mov	[n],3

     mov	eax,[mass]
     mov	ebx,4
     mul	ebx
     invoke	LocalAlloc,0,eax
     mov	dword[mass+4], eax

     cinvoke	printf,fmt3,str0
     cinvoke	gets,x
     cinvoke	atoi,x
     mov	[n],eax
     mov	[amount],0
     call	z

startz:
	cinvoke printf, fmt3, str1
	cinvoke gets, x
	cinvoke atoi, x

	cmp	eax, 0
	je	exit

	cmp	eax, 1
	jne	c2
	call	print
	jmp	startz
c2:
	cmp	eax, 2
	jne	c3

	cinvoke printf, fmt3,str0
	cinvoke gets, x
	cinvoke atoi, x
	push	eax
	call	ad
	jmp	startz
c3:
	call	del
	jmp	startz
exit:
	invoke LocalFree
	call [getch]
	invoke ExitProcess,0





proc	 renewn
locals
	a	dd 1
endl

renewn2:
	mov	eax,[a]
	cmp	[amount],eax
	jl	renewn1

	mov	ebx,2
	mul	ebx
	mov	[a],eax
	jmp	renewn2
renewn1:
	mov	eax,[a]
	mov	[mass],eax
ret
endp



proc	z
	cinvoke time,0
	cinvoke srand,eax
	mov	ecx,[n]
@@:
	push	ecx
	cinvoke rand
	mov	ebx,100
	xor	edx,edx
	div	ebx
	mov	[x],edx
	push	[x]

	call	ad
	pop	ecx
	loop	@b
ret
endp


proc	print
locals
	num	dd ?
	count	dd ?
	c	dd 1
	ecxs	dd ?
	am	dd 1
	form	dd ?
	nf	dd ?
endl
	mov	eax,[mass]
	mov	[num],eax
	mov	ebx,2
	xor	edx,edx
	div	ebx
	mov	[form],eax
	dec	[form]
	mov	eax,[mass+4]
	mov	[count],eax
print2:
	mov	ecx,[c]
	mov	[ecxs],ecx
	mov	eax,[form]
	mov	[nf], eax
	mov	eax,[num]
	add	[nf], eax
print1:
	mov	eax,[amount]
	cmp	[am], eax
	jg	print3

	mov	ecx,[nf]
@@:
	push	ecx
	cinvoke printf,fmt
	pop	ecx
	loop	@b

	mov	esi,[count]
	lodsd
	cinvoke printf,fmt1,eax
	add	[count],4

	mov	ecx,[nf]
@@:
	push	ecx
	cinvoke printf,fmt
	pop	ecx
	loop	@b

	cinvoke printf,fmt

	inc	[am]
	sub	[ecxs],1
	cmp	[ecxs],0
	jne	print1

	cinvoke printf,fmt2

	mov	eax,[c]
	mov	ebx,2
	mul	ebx
	mov	[c],eax

	dec	[form]
	mov	eax,[form]
	mov	ebx,2
	xor	edx,edx
	div	ebx
	mov	[form],eax

	mov	eax,[num]
	mov	ebx,2
	xor	edx,edx
	div	ebx
	mov	[num],eax

	cmp	[num],0
	jne	print2

print3:
ret
endp


proc	ad, val:DWORD
locals
	am	dd ?
	c1	dd ?
	c2	dd ?
	c3	dd ?
endl
	mov	eax,[amount]
	mov	[am],eax

	mov	ebx,4
	mul	ebx
	add	eax,[mass+4]
	mov	[c1],eax
	mov	[c2],eax
ad2:
	dec	[am]
	mov	eax,[am]
	mov	ebx,2
	xor	edx,edx
	div	ebx
	mov	[am],eax

	mov	ebx,4
	mul	ebx
	add	eax,[mass+4]
	mov	[c3],eax

	mov	esi,eax
	lodsd
	cmp	eax,[val]
	jg	ad3
	mov	edi,[c1]
	stosd

	mov	eax,[c3]
	mov	[c2],eax
	mov	[c1],eax
	jmp	ad2
ad3:
	mov	edi,[c2]
	mov	eax,[val]
	stosd

	inc	[amount]
	call	renewn
ret
endp


proc	del
locals
	am	dd ?
	aml	dd ?
	amr	dd ?
	c1	dd ?
	c2	dd ?
	c3	dd ?
	cl	dd ?
	cr	dd ?
	nl	dd ?
	nr	dd ?
	val	dd ?
endl
	dec	[amount]
	mov	eax,[amount]
	mov	ebx,4
	mul	ebx
	add	eax,[mass+4]

	mov	esi,eax
	lodsd
	mov	[val],eax
	mov	edi,[mass+4]
	stosd

	mov	[am],0
	mov	eax,[mass+4]
	mov	[c1],eax
	mov	[c2],eax
del2:
	mov	eax,[am]
	mov	ebx,2
	mul	ebx
	inc	eax
	mov	[aml],eax
	inc	eax
	mov	[amr],eax

	sub	eax,1
	mov	ebx,4
	mul	ebx
	add	eax,[mass+4]
	mov	[cl],eax
	add	eax,4
	mov	[cr],eax

	mov	eax,[aml]
	cmp	eax,[amount]
	jg	del3
	inc	eax
	cmp	eax,[amount]
	jge	delt4

	mov	esi,[cl]
	lodsd
	mov	[nl],eax
	lodsd
	mov	[nr],eax
	cmp	[nl],eax
	jg	delt4

	mov	eax,[cr]
	mov	[c3],eax
	mov	eax,[amr]
	mov	[am],eax
	jmp	delt5
delt4:
	mov	eax,[cl]
	mov	[c3],eax

	mov	eax,[aml]
	mov	[am],eax
delt5:
	mov	esi,[c3]
	lodsd
	cmp	eax,[val]
	jl	del3

	mov	edi,[c1]
	stosd

	mov	eax,[c3]
	mov	[c2],eax
	mov	[c1],eax
	jmp	del2
del3:
	mov	edi,[c2]
	mov	eax,[val]
	stosd

	call	renewn
ret
endp		 



section '.idata' import data readable

	library kernel, 'kernel32.dll',\
		msvcrt, 'msvcrt.dll'

	import kernel,\
	       ExitProcess, 'ExitProcess',\
	       LocalAlloc, 'LocalAlloc',\
	       LocalFree, 'LocalFree'

	import msvcrt,\
	       strlen, 'strlen',\
	       printf, 'printf',\
	       atoi, 'atoi',\
	       gets, 'gets',\
	       getch, '_getch',\
	       time, 'time',\
	       srand, 'srand',\
	       rand, 'rand'